[TestMethod]
public void TestMaj() {

    Assert.AreEqual(true, Program.DebutMaj("Bonjour à tous"));
    Assert.AreEqual(false, Program.DebutMaj("salut les amis"));
}

[TestMethod]
public void TestPoint() {

    Assert.AreEqual(false, Program.FinPoint("coucou les copains"));
    Assert.AssEqual(true, Program.FinPoint("slt lé kopin."))

}
